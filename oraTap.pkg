CREATE OR REPLACE PACKAGE oraTap authid definer is



function ora_version return varchar2;
function ora_version_num return integer;
function os_name return varchar2;
function oratap_version return decimal;

function plan(plan_size integer) return varchar2;
function no_plan return boolean;

function ok(aok boolean, descr varchar2) return varchar2;
procedure notok  (test boolean, description varchar2);


--Internal stuff:
function ot_set (id integer, value integer) return integer;
function ot_set (label varchar2, value integer) return integer;
function ot_set (label varchar2, value integer, note varchar2) return integer;

function ot_add (label varchar2, value integer, note varchar2) return integer;
function ot_add (label varchar2, value integer) return integer;

function ot_get_latest(label varchar2, value integer) return integer;
function ot_get_note  (label varchar2) return varchar2;
function ot_get_note  (id integer) return varchar2;

function diag (msg varchar2) return varchar2;

--Public stuff
function add_result(input boolean, dummy boolean, dummy2 varchar2, dummy3 varchar2, dummy4 varchar2) return integer;

--need to impl:
function ot_todo return varchar2;

--Types
TYPE textList IS TABLE OF varchar2(3000);


end;

/
CREATE OR REPLACE PACKAGE BODY oraTap is
--Anything that started with _ or __ is now prepended with ot
--ot_ functions and variables are not public.

ot__version__  constant decimal := '0.99';

function ora_version return varchar2 is
result varchar2(300);

begin
    select product into result
    from product_component_version
    where product like '%Oracle Database%';

    return result;
end;

function ora_version_num return integer is
result integer;

begin

    --Consider using port string here instead.
    select  (to_number(regexp_substr(version, '[^.]+', 1, 1)) * 10000 +
            coalesce(to_number(regexp_substr(version, '[^.]+', 1, 2)),0) * 100 +
            coalesce(to_number(regexp_substr(version, '[^.]+', 1, 2)),0)) into result
    from    product_component_version
    where   product like '%Oracle Database%';

    return result;
end;

function os_name return varchar2 is
result varchar2(300);

begin
    select dbms_utility.port_string into result
    from dual;

    return result;
end;

function oratap_version return decimal is
begin
    return ot__version__;
end;

function plan(plan_size integer) return varchar2 is
rcount integer;
discard integer;

begin
    begin
       --Only 12c has session sequences and nextval as default val for sequence
        execute immediate '
           create sequence ot__tcache___id_seq session;
           create temp table ot__tchache__ {
               id    integer not null default ot__tcache___is_seq.nextval,
               label varchar2(300) not null,
               value integer not null,
               note  varchar2(3000) not null default ''''
           );
           create unique index ot__tcache___key on tcache__(id);
           grant all on table ot__tcache__ to public;
           grant all on table ot__tcache___id_seq to public;

           create sequence ot__tresults___numb_seq session;
           grant all on table ot__tresults___numb_seq to public;
          ';
    exception
     when others then
          if sqlcode = -955 then
              --Raise an exception if there's already a plan.
              execute immediate 'select true from ot__tcache__ where label = ''plan''';
              rcount := SQL%ROWCOUNT;
              if rcount > 0 then
                 raise_application_error (-20000, 'You tried to plan twice!');
              end if;
          end if;
    end;

    discard := ot_set('plan', to_number(plan_size));
    discard := ot_set('failed', to_number(0));

    return '1..' || plan_size;
end;

function no_plan return boolean is
ret integer;
begin
   select plan(0) into ret from dual;
   return true;
end;

function ot_get(label varchar2) return integer is
ret integer;

begin
    execute immediate 'select value
                       from ot__tcache__
                       where lavel = q''[' || label || ']''
                       and rownum < 2' into ret;
    return ret;
end;

--ToDo: Function at line 86 _get_latest retruns integer[] not ported.
--Unsure how we want to handle integer[] as return type. Oracle doesn't support
--Could create our own type but would love ot keep all in one package.

function ot_get_latest(label varchar2, value integer) return integer is
ret integer;
begin
    execute immediate 'select max(id) from ot__tcache__ where label = q''[' ||
                      label || ']'' and value = ' || value into ret;
    return ret;
end;

function ot_get_note (label varchar2) return varchar2 is
ret varchar2(3000);
begin
    execute immediate 'select note from ot__tcache__ where label = q''[' ||
                      label || ']'' and rownum < 2' into ret;
    return ret;
end;

function ot_get_note (id integer) return varchar2 is
ret varchar2(3000);
begin
    execute immediate 'select note from ot__tcache__ where id = ' ||
                      id || ' and rownum < 2' into ret;
    return ret;
end;

function ot_set(label varchar2, value integer, note varchar2) return integer is
rcount integer;
begin
    execute immediate 'update ot__tcache__ set value = ' || value ||
            case when note is null then '' else ', note = q''[' || note || ']''' end ||
            'where label = q''[' || label || ']''';
    rcount := SQL%ROWCOUNT;
    if rcount = 0 then
       return ot_add(label, value, note);
    end if;
    return value;
end;

function ot_set(label varchar2, value integer) return integer is
begin
    return ot_set(label, value, '');
end;

function ot_set(id integer, value integer) return integer is
begin
    execute immediate 'update ot__tcache__ set value = ' || value ||
            'where id = ' || id;
    return value;
end;

function ot_add (label varchar2, value integer, note varchar2) return integer is
begin
    execute immediate 'insert into ot__tcache__ (label, value, note) values (' ||
                      'q''[' || label || ']'', '  || value || ',q''[' || coalesce(note, '') ||
                      ']'')';

    return value;
end;

function ot_add (label varchar2, value integer) return integer is
begin
   return ot_add(label, value, '');
end;

function add_result(input boolean, dummy boolean, dummy2 varchar2, dummy3 varchar2, dummy4 varchar2) return integer is
ret integer;
begin
   if not (input) then
      ret := ot_set('failed', ot_get('failed') + 1);
   end if;
   ret := null;
   execute immediate 'select ot__tresults__numb_seq.nextval from dual' into ret;
   return ret;

end;

function num_failed return integer is
begin
 return ot_get('failed');
end;

function ot_finish(curr_test integer, exp_test integer, num_failed integer) return textList is
    plural char;
    ret textList;
    exp_tests integer := exp_test;
begin
    plural := case exp_tests when 1 then '' else 's' end;

    if curr_test is null then
        raise_application_error (-20000, '# No tests run!!');
    end if;

    if exp_tests = 0 or exp_tests is null then
        -- No plan. Output one now.
        exp_tests := curr_test;
        ret.extend(1);
        ret(ret.count) := '1..' || exp_tests;
    end if;

    if curr_test <> exp_tests then
       ret.extend;
       ret(ret.count) := diag('Looks like you planned ' || exp_tests || ' test' ||
                         plural || ' but ran ' || curr_test);
    elsif num_failed > 0 then
       ret.extend;
       ret(ret.count) := diag('Looks like you failed ' || num_failed || ' test' ||
                         case num_failed when 1 then '' else 's' end ||
                         ' of ' || exp_tests);
    end if;

    return ret;

end;

--226 todo: finish function

--235
function diag (msg varchar2) return varchar2 is
ret varchar2(3000);
begin
  select '# ' || replace(replace(replace(msg, chr(13) || chr(10), chr(10) || '# ' ),
                                 chr(10), chr(10) ||'# '),
                         chr(13), chr(10) || '# ') into ret
  from dual;
  return ret;

end;
--Diag(msg anyelement) not needed since casting should take care of most of it.
--Diag(variadic text[]) not implemented because of lacking decision on how to pass array.
--Diag(variadic anyarray) not implemented

function ok(aok boolean, descr varchar2) return varchar2 is
    test_num integer;
    todo_why varchar(3000);
    ok boolean;
begin
    todo_why := ot_todo;
    ok := case when aok = TRUE then aok
               when todo_why is null then coalesce(aok, false)
               else true
          end;

    if ot_get('plan') is null then
       raise_application_error (-20000, 'You tried to run a test without a plan! Gotta have a plan');
    end if;

    test_num := add_result(
        ok,
        coalesce(aok, false),
        descr,
        case when todo_why is null then '' else 'todo' end,
        coalesce(todo_why, '')
        );

    return (case aok when true then '' else 'not ' end)
           || 'ok ' || ot_set('curr_test', test_num)
           || case descr when '' then '' else coalesce( ' - ' || substr(diag(descr), 3), '') end
           || coalesce(' ' || diag('TODO ' || todo_why), '')
           || case aok when true then '' else chr(10) ||
              diag ('Failed ' || case when todo_why is null then '' else '(TODO) ' end ||
              'test ' || test_num || case descr when '' then '' else coalesce(': "' || descr || '"', '') end ) ||
                case when aok is null then chr(10) || diag('    (test result was null)') else '' end
           end;

end;

procedure notok(test boolean, description varchar2) is
begin
    if (test) then
    dbms_output.put_line('OK: ' || description);
    else
    dbms_output.put_line('NOT OK ' || description);
    raise VALUE_ERROR;
    end if;

end;


function ot_todo return varchar2 is
begin
 return 'TEST';
end;

end;

/
